<?php

class ViewWarehouses {
    protected $_model = null;

    public function __construct()
    {
        $this->_model = new Warehouses();
    }

    public function showTable()
    {
        $html = '<form action="' . $_SERVER["PHP_SELF"] . '" method="post" enctype="multipart/form-data">
            <table border="1" width="600">
            <tr>
            <td width="40%">product_name</td>
            <td width="30%">qty</td>
            <td width="30%">warehouse</td>
            </tr>' .
            $this->_getProductsHtml() .
            '<tr>
            <td width="20%">Select file</td>
            <td colspan="2" width="80%"><input type="file" name="file" id="file" /></td>
            </tr>
            <tr>
            <td>Submit</td>
            <td colspan="2"><input type="submit" name="submit" /></td>
            </tr>
            </table>
            </form>';
        echo $html;
    }

    protected function _getProductsHtml()
    {
        $html = '';
        $productRows = $this->_model->getProductsDB();
        if (empty($productRows)) {
            return '<tr><td colspan="3">No products. Import to see anything here. </td></tr>';
        }
        foreach ($productRows as $product) {
            $html .= "<tr><td>{$product['name']}</td>
                    <td>{$product['qty']}</td>
                    <td>{$product['warehouses']}</td></tr>";
        }
        return $html;
    }
}