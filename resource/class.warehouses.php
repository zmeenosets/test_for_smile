<?php

class Warehouses
{
    public function __construct()
    {
        //Init Tables first
        if(!$this->_initTables()) {
            die ('Tables are not initialized');
        }
        //Check if new csv file uploaded than update DB
        $updateValues = $this->_getUploadCsv();
        if (!empty($updateValues)) {
            $this->_setProductsDB($updateValues);
        }
    }

    /**
     * Get all products with warehouses
     * @return array
     */
    public function getProductsDB()
    {
        $db = DB::getConnection();
        $productRows = array();
        try {
            $query = $db->query("SELECT SUM(wh.qty) AS qty, p.name, GROUP_CONCAT(wh.code SEPARATOR ', ') AS warehouses FROM warehouses AS wh
            INNER JOIN products AS p ON p.id = wh.product_id
            GROUP BY wh.product_id");
            if ($query->rowCount() > 0) {
                $productRows = $query->fetchAll();
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
        return $productRows;
    }

    /**
     * First init all tables
     * @return bool
     */
    protected function _initTables()
    {
        $create_products_tbl_sql = "
            CREATE TABLE IF NOT EXISTS products (
              id INT(10) NOT NULL AUTO_INCREMENT,
              name VARCHAR(255) DEFAULT NULL,
              PRIMARY KEY (id)
            ) ENGINE=InnoDB";

        $create_warehouses_tbl_sql = "
            CREATE TABLE IF NOT EXISTS warehouses (
              id INT(10) NOT NULL AUTO_INCREMENT,
              qty INT(12) DEFAULT NULL,
              code VARCHAR(50) NOT NULL,
              product_id INT(10) NOT NULL,
              PRIMARY KEY (id),
              FOREIGN KEY (product_id) REFERENCES products (id)
              ON UPDATE RESTRICT
              ON DELETE CASCADE
            ) ENGINE=InnoDB";

        if($this->_checkTable('Products', $create_products_tbl_sql) &&
            $this->_checkTable('Warehouses', $create_warehouses_tbl_sql)) {
            return true;
        }

        return false;
    }

    /**
     * Check need create table
     * @param $tableName
     * @param $insertSql
     * @return bool
     */
    protected function _checkTable($tableName, $insertSql)
    {
        $db = DB::getConnection();
        try {
            $result = $db->exec($insertSql);
            if ($result === false) {
                throw new PDOException ("Error creating " . $tableName . " table.");
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            return false;
        }
        return true;
    }

    /**
     * Parse uploaded csv file if exist
     * @return array
     */
    protected function _getUploadCsv()
    {
        $result = array();
        try {
            if (isset($_FILES["file"])) {
                if ($_FILES["file"]["error"] > 0) {
                    throw new Exception ("File download error. Code: " . $_FILES["file"]["error"]);
                } else {
                    $file = $_FILES["file"]["tmp_name"];
                    if (($handle = fopen($file, "r")) !== FALSE) {
                        $row = 0;
                        while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                            if ($data[0] != 'product_name' && count($data) >= 3) {
                                $result[$row]['name'] = $data[0];
                                $result[$row]['qty'] = $data[1];
                                $result[$row]['code'] = $data[2];
                                $row++;
                            }
                        }
                        fclose($handle);
                    }
                }
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }
        return $result;
    }

    /**
     * Update/Insert values to DB
     * @param array $values
     */
    protected function _setProductsDB(array $values)
    {
        $db = DB::getConnection();
        try {
            $db->beginTransaction();
            foreach ($values as $row) {
                $prevRow = $this->_getPrevRowDB($row);
                //Insert
                if(empty($prevRow) || $prevRow['id'] <= 0) {
                    $qty = (int)$row['qty'] > 0 ? (int)$row['qty'] : 0;
                    $sql = "INSERT INTO products (name) VALUES (?)";
                    $stmt = $db->prepare($sql);
                    $stmt->execute(array($row['name']));
                    $sql2 = "INSERT INTO warehouses (qty, code, product_id)
                  VALUES (?, ?, LAST_INSERT_ID())";
                    $stmt = $db->prepare($sql2);
                    $stmt->execute(array($qty, $row['code']));
                }
                //Update
                else {
                    $qty = (int)$row['qty'] + (int)$prevRow['qty'];
                    if($qty < 0) {
                        $qty = 0;
                    }
                    $sql = "UPDATE warehouses SET qty=? WHERE id=?";
                    $stmt = $db->prepare($sql);
                    $stmt->execute(array($qty, (int)$prevRow['id']));
                }
            }
            $db->commit();
        } catch (PDOException $e) {
            $db->rollBack();
            echo $e->getMessage();
        }
    }

    /**
     * Fetch previous row from existing in DB
     * @param $row - exist row
     * @return array
     */
    protected function _getPrevRowDB($row)
    {
        $db = DB::getConnection();
        $result = array();

        $sql = "SELECT wh.* FROM warehouses AS wh
          INNER JOIN products AS p ON p.id = wh.product_id
          WHERE p.name = ? AND wh.code = ?";
        $stmt = $db->prepare($sql);
        $status = $stmt->execute(array($row['name'], $row['code']));
        if($status === true) {
            $result = $stmt->fetch();
        }
        return $result;
    }
}