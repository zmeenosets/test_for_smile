<?

class DB
{
    private static $_db;

    private function __construct() {}

    private function __clone() {}

    public static function getConnection()
    {
        if (self::$_db == null) {
            try {
                self::$_db = new PDO('mysql:host=' . DB_HOST . ';charset=' . DB_CHARSET, DB_USER, DB_PASS);
                self::$_db->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
                self::$_db->exec('CREATE DATABASE IF NOT EXISTS ' . DB_NAME);
                self::$_db->exec('use ' . DB_NAME);
            } catch (PDOException $e) {
                die('<h2>Connection is unavailable.</h2>' . $e->getMessage());
            }
            return self::$_db;
        } else {
            return self::$_db;
        }
    }
}