<?php
error_reporting(E_ALL);

require 'resource/class.db.php';
require 'resource/class.warehouses.php';
require 'class.view.warehouses.php';
define('DB_HOST', 'localhost');
define('DB_USER', 'root');
define('DB_PASS', '');
define('DB_NAME', 'test2');
define('DB_CHARSET', 'utf8');

$warehouses = new ViewWarehouses();
$warehouses->showTable();